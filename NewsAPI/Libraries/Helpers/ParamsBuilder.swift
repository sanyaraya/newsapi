//
//  QueryBuilder.swift
//  FoamyCoolness
//
//  Created by Александр on 22.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit

class ParamsBuilder: NSObject {
    // MARK: - Constants

    private enum Keys {
        static let Page = "p"
        static let Query = "q"
        static let Name = "name"
        static let APIKey = "api-key"
        static let Latitude = "lat"
        static let Longtitude = "lng"
        static let SearchType = "type"
        static let SearchRadius = "radius"

        static let TypeBeer = "Beer"
        static let TypeBrewery = "Brewery"
    }

    // MARK: - Params

    private var params: [String : String] = [Keys.APIKey : API.APIKey]

    // MARK: - Building

    func forPage(page: Int) -> ParamsBuilder {
        params[Keys.Page] = "\(page)"
        return self
    }

    func search(text: String) -> ParamsBuilder {
        if text.count > 0 {
            params[Keys.Query] = "\(text)"
        }
        return self
    }

    func searchCircle(radius: Int) -> ParamsBuilder {
        params[Keys.SearchRadius] = "\(radius)"
        return self
    }

    func searchBeer() -> ParamsBuilder {
        params[Keys.SearchType] = Keys.TypeBeer
        return self
    }

    func build() -> [String: String] {
        return self.params
    }
}

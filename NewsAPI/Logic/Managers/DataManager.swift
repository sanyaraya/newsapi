//
//  DataManager.swift
//  NewsAPI
//
//  Created by Александр on 7/11/19.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxSwift
import RxAlamofire
import Alamofire
import MoreCodable

class DataManager: NSObject {

    //MARKL - Constants
    
    let baseUrl = "https://api.nytimes.com/svc/"

    enum Packages {
        static let topStories = "topstories/v2/"
        static let articlesSearch = "search/v2/articlesearch"
    }
    //MARK: - Properties

    static let shared = DataManager()

    let afManager = SessionManager.default

    //MARK: - Articles
    
    func getSearchedArticles(query: String) -> Observable<[SearchedArticle]> {
        return self.articlesForQuery(query: query).map({ (res, data) in
            let json = data as? [String : Any]
            let resp = json?["response"] as? [String: Any]
            
            guard let objects = resp?["docs"] as? [[String : Any]] else {
                return []
            }
            
            let results = objects.compactMap{
                try? DictionaryDecoder().decode(SearchedArticle.self, from: $0)
            }
            
            return results
        })
    }

    
    func articlesForQuery(query: String) -> Observable<(HTTPURLResponse, Any)> {
        let params = ParamsBuilder().search(text: query).build()
        return self.executeURL(url: self.baseUrl + Packages.articlesSearch + ".json", params: params, method: .get)
    }
    
    func getArticlesForSection(section: String) -> Observable<[Article]> {
        return self.articlesForSection(section: section).map({ (response, data)  in
            let json = data as? [String : Any]
            
            guard let info = json?["results"] as? [[String : Any]] else {
                return []
            }
        
            let results = info.compactMap{
                try? DictionaryDecoder().decode(Article.self, from: $0)
            }
           
            return results
        })
    }
    
    func articlesForSection(section: String) -> Observable<(HTTPURLResponse, Any)> {
        let params = ParamsBuilder().build()
        return self.executeURL(url: self.baseUrl + Packages.topStories + section + ".json", params: params, method: .get)
    }
    //MARK: - Execution
    
    func executeURL(url: String, params: [String : Any], method: Alamofire.HTTPMethod) -> Observable<(HTTPURLResponse, Any)> {
        return afManager.rx.responseJSON(method, url, parameters: params)
    }
}

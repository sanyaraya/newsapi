//
//  ImageLoader.swift
//  NewsAPI
//
//  Created by Александр on 7/12/19.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ImageLoader: NSObject {
        
    // MARK: - loading
        
    func loadImageUrl(url: URL) -> Observable<UIImage?> {
        return URLSession.shared.rx.data(request: URLRequest(url: url))
            .map { data in
                return UIImage(data: data)
            }.catchErrorJustReturn(nil)
    }
}
    


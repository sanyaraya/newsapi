//
//  Article.swift
//  NewsAPI
//
//  Created by Александр on 7/11/19.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation


struct Article: Decodable {
    //MARK: - Constants
    
    enum Keys: String, CodingKey {
        case author = "byline"
        case section
        case title
        case url
        case photos = "multimedia"
        case description = "abstract"
    }
    
    //MARK: - Properties
    
    let author: String
    let section: String
    let title: String
    let url: String
    let description: String
   
    var photos: [PhotoModel]?
    
    //MARK: - Initializers
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Keys.self)
        self.author = try container.decode(String.self, forKey: .author)
        self.section = try container.decode(String.self, forKey: .section)
        self.title = try container.decode(String.self, forKey: .title)
        self.photos = try? container.decode([PhotoModel].self, forKey: .photos)
        self.description = try container.decode(String.self, forKey: .description)
        self.url = try container.decode(String.self, forKey: .url)
    }
    
    //MARK: - Helper
    
    func getImageUrl() -> String? {
        guard let images = self.photos, images.count > 0 else {
            return nil
        }
        
        let middle = images[images.count / 2]
        
        if middle.type == "image" {
            return middle.url
        }
        
        for i in stride(from: images.count - 1, through: 0, by: -1) {
            if images[i].type == "image" {
                return images[i].url
            }
        }
        
        return nil
    }
}

struct PhotoModel: Decodable {
    let url: String
    let type: String
}

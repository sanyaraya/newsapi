//
//  File.swift
//  NewsAPI
//
//  Created by Александр on 7/12/19.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation

struct SearchedArticle: Decodable {
    
    struct HeadLine: Decodable {
        let main: String
    }
    
    struct ByLine: Decodable {
        let original: String
    }

    
    //MARK: - Constants
    
    enum Keys: String, CodingKey {
        case byline = "byline"
        case headline
        case url = "web_url"
        case photos = "multimedia"
        case description = "abstract"
    }
    
    let photoPrefix = "https://static01.nyt.com/"
    
    //MARK: - Properties
    
    let url: String
    let description: String
    
    let headlLine: HeadLine
    let byline: ByLine
    
    var photos: [PhotoModel]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Keys.self)
        self.headlLine = try container.decode(HeadLine.self, forKey: .headline)
        self.photos = try? container.decode([PhotoModel].self, forKey: .photos)
        self.description = try container.decode(String.self, forKey: .description)
        self.url = try container.decode(String.self, forKey: .url)
        self.byline = try container.decode(ByLine.self, forKey: .byline)
    }
    
    func getImageUrl() -> String? {
        guard let images = self.photos, images.count > 0 else {
            return nil
        }
        
        let middle = images[images.count / 2]
        
        if middle.type == "image" {
            return middle.url.hasPrefix("http") ? middle.url : photoPrefix + middle.url
        }
        
        for i in stride(from: images.count - 1, through: 0, by: -1) {
            if images[i].type == "image" {
               return images[i].url.hasPrefix("http") ? images[i].url : photoPrefix + images[i].url
            }
        }
        
        return nil
    }
}



//
//  ArticleCell.swift
//  NewsAPI
//
//  Created by Александр on 7/11/19.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxSwift

class ArticleCell: UITableViewCell {

    //MARK: - Properties
    
    @IBOutlet weak var ivArt: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription:
    UILabel!
    @IBOutlet weak var aiView: UIActivityIndicatorView!
    
    private let loader = ImageLoader()
    
    var disposeBag = DisposeBag()
    
    var article: ArticleType? {
        didSet {
            guard let art = self.article else {
                return
            }
            switch art {
            case .searchedArticle(let searched):
                self.loadSearchedArticle(article: searched)
            case .sectionArticle(let article):
                self.loadSectionedArticle(article: article)
            }
        }
    }
    
    //MARK: - Lifecycle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.disposeBag = DisposeBag()
    }
    
    //MARK: - loading models
 
    private func loadSectionedArticle(article: Article) {
        self.lblTitle.text = article.title
        self.lblDescription.text = article.description
        self.loadImage(url: article.getImageUrl())
    }
    
    private func loadSearchedArticle(article: SearchedArticle) {
        self.lblTitle.text = article.headlLine.main
        self.lblDescription.text = article.description
        self.loadImage(url: article.getImageUrl())
    }

    
    func loadImage(url: String?) {
        self.ivArt.image = UIImage(named: "news.png")
        if let urlString = url, let urlReq = URL(string: urlString) {
            self.aiView.startAnimating()
            self.loader.loadImageUrl(url: urlReq).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self](image) in
                if let img = image {
                    self?.ivArt.image = img
                }
                self?.aiView.stopAnimating()
            }).disposed(by: self.disposeBag)
        }
    }
    
}

extension ArticleCell: AutoIndentifierCell {}

//
//  ArticleViewController.swift
//  NewsAPI
//
//  Created by Александр on 7/12/19.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

//MARK: - ArticleType

enum ArticleType {
    case sectionArticle(Article)
    case searchedArticle(SearchedArticle)
}


class ArticleViewController: UIViewController {

    //MARK: - Properties
    
    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var lblArticle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var ivPhoto: UIImageView!
    @IBOutlet weak var btnFull: UIButton!
    
    let disposeBag = DisposeBag()
    
    var article: ArticleType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadUI()
    }
    
    //MARK: - Instance
    
    static func getInstance(article: ArticleType) -> ArticleViewController {
        let vc = ArticleViewController.storyboardInstance()!
        vc.article = article
        return vc
    }
    
    //MARK: - loading models
    
    private func loadUI() {
        guard let article = self.article else {
            return
        }
        
        switch article {
        case .searchedArticle(let searched):
            self.loadSearched(article: searched)
        case .sectionArticle(let section):
            self.loadSectionArticle(article: section)
        }
    }
    
    private func loadSectionArticle(article: Article) {
        self.lblTitle.text = article.title
        self.lblArticle.text = article.description
        self.lblAuthor.text = article.author
        
        self.loadPhoto(urlString: article.getImageUrl())
        self.setButtonLink(url: article.url)
    }
    
    private func loadSearched(article: SearchedArticle) {
        self.lblTitle.text = article.headlLine.main
        self.lblArticle.text = article.description
        self.lblAuthor.text = article.byline.original
        self.loadPhoto(urlString: article.getImageUrl())
        self.setButtonLink(url: article.url)
    }

    private func setButtonLink(url: String) {
        self.btnFull.rx.tap.subscribe(onNext: { (_) in
            guard let url = URL(string: url) else {
                return
            }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }).disposed(by: self.disposeBag)
    }
    
    private func loadPhoto(urlString: String?) {
        self.ivPhoto.image = UIImage(named: "news.png")
        
        if let url = urlString, let urlReq = URL(string: url) {
            ImageLoader().loadImageUrl(url: urlReq).observeOn(MainScheduler.instance).subscribe(onNext: {[weak self] (img) in
                if let image = img {
                    self?.ivPhoto.image = image
                }
            }).disposed(by: self.disposeBag)
        }
    }
}

//MARK: - StoryboardInstantiable

extension ArticleViewController: StoryboardInstantiable {
    static var storyboardName: String {
        return Storyboards.Article
    }
}

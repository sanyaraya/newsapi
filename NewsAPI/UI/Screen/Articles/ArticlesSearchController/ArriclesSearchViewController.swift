//
//  ArriclesSearchViewController.swift
//  NewsAPI
//
//  Created by Александр on 7/12/19.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ArticlesSearchViewController: UIViewController {
    
    //MARK: - Constants
    
    private enum Defaluts {
        static let cellHeightRatio: CGFloat = 1 / 3
    }
    
    @IBOutlet weak var aiView: UIActivityIndicatorView!
    @IBOutlet weak var sbSearch: UISearchBar!
    @IBOutlet weak var tvArticles: UITableView!
    
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.navigationItem.title = "Searched Articles"
        self.registerCell()
        self.bindTable()
    }
    
    //MARK: - Binding
    
    private func bindTable() {
        let searchInput = self.sbSearch.rx.text.throttle(.milliseconds(500), scheduler: MainScheduler.instance).distinctUntilChanged()
        
        let search = searchInput.flatMap {  query -> Observable<[SearchedArticle]> in
            return DataManager.shared.getSearchedArticles(query: query ?? "")
        }
        
        let running = Observable.from([searchInput.map{_ in true},
                                       search.map{_ in false}.asObservable()
            ]).merge()
        
        
        running.asDriver(onErrorJustReturn: false)
            .drive(self.aiView.rx.isAnimating)
            .disposed(by: self.disposeBag)
        
        search.bind(to: self.tvArticles.rx.items(cellIdentifier: ArticleCell.identifier, cellType: ArticleCell.self)) {
            index, model, cell in
            cell.article = .searchedArticle(model)
            }.disposed(by: self.disposeBag)
        
        self.tvArticles.delegate = self
        
        self.tvArticles.rx.modelSelected(SearchedArticle.self).subscribe(onNext: { (model) in
            self.navigationController?.pushViewController(ArticleViewController.getInstance(article: .searchedArticle(model)), animated: true)
        }).disposed(by: self.disposeBag)
    }
    
    //MARK: - Cells
    
    private func registerCell() {
        self.tvArticles.register(UINib(nibName: ArticleCell.nibName, bundle: nil), forCellReuseIdentifier: ArticleCell.identifier)
    }
}

//MARK: - UITableViewDelegate

extension ArticlesSearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.width / 3
    }
}

//MARK: - StoryboardInstantiable

extension ArticlesSearchViewController: StoryboardInstantiable {
    static var storyboardName: String {
        return Storyboards.Sections
    }
}

//
//  ArticlesViewController.swift
//  NewsAPI
//
//  Created by Александр on 7/11/19.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxSwift

class ArticlesViewController: UIViewController {
    
    //MARK: - Properties
    
    var sectionName: String?
    
    var disposeBag = DisposeBag()
    
    @IBOutlet weak var tvContent: UITableView!
    
    @IBOutlet weak var aiView: UIActivityIndicatorView!
    
    //MARK: - Instance
    
    static func getInstance(section: String) -> ArticlesViewController {
        let vc = ArticlesViewController.storyboardInstance()
        vc?.sectionName = section
        return vc!
    }
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCell()
        
        self.navigationItem.title = (self.sectionName ?? "Section Name").capitalizingFirstLetter()
        
        bindTable()
    }
    
    //MARK: - Binding
    
    private func bindTable() {
        let articles = DataManager.shared.getArticlesForSection(section: sectionName!)
        
        articles.observeOn(MainScheduler.instance).subscribe { (event) in
            self.aiView.stopAnimating()
            }.disposed(by: self.disposeBag)
        
        articles.bind(to: self.tvContent.rx.items(cellIdentifier:ArticleCell.identifier, cellType: ArticleCell.self)) {
            index, model, cell in
            cell.article = .sectionArticle(model)
            }.disposed(by: self.disposeBag)
        
        self.tvContent.delegate = self
        
        self.tvContent!.rx.modelSelected(Article.self).subscribe(onNext: { (model) in
            self.navigationController?.pushViewController(ArticleViewController.getInstance(article: ArticleType.sectionArticle(model)), animated: true)
        }).disposed(by: self.disposeBag)
    }
    
    private func registerCell() {
        self.tvContent.register(UINib(nibName: ArticleCell.nibName, bundle: nil), forCellReuseIdentifier: ArticleCell.identifier)
    }
    
}

//MARK: - StoryboardInstantiable

extension ArticlesViewController: StoryboardInstantiable {
    static var storyboardName: String {
        return Storyboards.Sections
    }
}

//MARK: - UITableViewDelegate

extension ArticlesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.width / 3
    }
}

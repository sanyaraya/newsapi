//
//  SectionsViewController.swift
//  NewsAPI
//
//  Created by Александр on 7/11/19.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SectionsViewController: UIViewController {
    
    //MARK: - Constants
    
    @IBOutlet weak var tvSections: UITableView!
    
    let sections = Observable.just(["arts", "automobiles", "books", "business", "fashion", "food", "health", "home", "insider", "magazine", "movies", "national", "nyregion", "obituaries", "opinion", "politics", "realestate", "science", "sports", "sundayreview", "technology", "theater", "tmagazine", "travel", "upshot", "world"])
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "View Sections"
        
        self.bindTable()
    }
    
    //MARK: - Binding
    
    private func bindTable() {
        self.sections.bind(to: self.tvSections.rx.items(cellIdentifier: "Cell")) {
            index, model, cell in
            cell.textLabel?.text = model
            }.disposed(by: self.disposeBag)
        
        self.tvSections!.rx.modelSelected(String.self).subscribe(onNext: { (model) in
            self.navigationController?.pushViewController(ArticlesViewController.getInstance(section: model), animated: true)
        }).disposed(by: self.disposeBag)
    }
    
}

//MARK: - StoryboardInstantiable

extension SectionsViewController: StoryboardInstantiable {
    static var storyboardName: String {
        return Storyboards.Sections
    }
}
